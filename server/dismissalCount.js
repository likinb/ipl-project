const csvToJson = require('csvtojson');
const fileSystem = require('fs');

var dismissalCount = {};

exports.batsmanDismissalCount = function (inputFilePath, outputFilePath) {
    csvToJson().fromFile(inputFilePath).then(matchesData => {
        matchesData.forEach(match => {
            var batsmanName = match['player_dismissed'];
            var bowlerName = match['bowler'];
            if (batsmanName !== '') {
                if (dismissalCount[batsmanName]) {
                    if (dismissalCount[batsmanName][bowlerName]) {
                        dismissalCount[batsmanName][bowlerName] += 1;
                    } else {
                        dismissalCount[batsmanName][bowlerName] = 1;
                    }
                } else {
                    dismissalCount[batsmanName] = {};
                    dismissalCount[batsmanName][bowlerName] = 1;
                }
            }
        })
        fileSystem.writeFile(outputFilePath, JSON.stringify(dismissalCount), function (err) {
            if (err) return console.log(err);
            console.log('Count of batsman dismissed by a bowler is updated at ' + outputFilePath);
        });
    })
}