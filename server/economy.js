const csvToJson = require('csvtojson');
const fileSystem = require('fs');


exports.top10Bowlers = function (inputFilePath, inputFileDeliveries, outputFilePath) {
    var economyOfBowlers = {};
    var matchIDs = [];

    csvToJson({ checkType: true }).fromFile(inputFilePath).then(matchesData => {
        matchesData.forEach(match => {
            if (match['season'] == 2015) {
                matchIDs.push(match['id']);
            }
        })
        var matchId = 0;
        csvToJson({ checkType: true }).fromFile(inputFileDeliveries).then(runData => {
            runData.forEach(ball2Ball => {
                matchId = ball2Ball['match_id'];
                if (matchIDs.includes(matchId)) {
                    var bowlerName = ball2Ball['bowler'];
                    var totalRuns = ball2Ball['total_runs'] - ball2Ball['bye_runs'] - ball2Ball['legbye_runs'];
                    if (economyOfBowlers[bowlerName] && economyOfBowlers[bowlerName] != null) {
                        economyOfBowlers[bowlerName]['runs'] += totalRuns;
                        if (ball2Ball['wide_runs'] == 0 && ball2Ball['noball_runs'] == 0) {
                            economyOfBowlers[bowlerName]['balls'] += 1;
                        }
                    } else {
                        economyOfBowlers[bowlerName] = {
                            'runs': totalRuns,
                            'balls': 1
                        };
                    }

                }
            })
            var economyArr = [];
            for (var bowlerName in economyOfBowlers) {
                var o = economyOfBowlers[bowlerName].balls;
                var over = o / 6;
                var economy = economyOfBowlers[bowlerName].runs / over;
                economyArr.push({ BowlerName: bowlerName, Economy: economy });
            }
            economyArr = economyArr.sort(function (a, b) { return a.Economy - b.Economy; });
            economyArr = economyArr.slice(0, 10);
            fileSystem.writeFile(outputFilePath, JSON.stringify(economyArr), function (err) {
                if (err) return console.log(err);
                console.log('Top 10 economical bowlers in 2015 is updated at ' + outputFilePath);
            });
        })

    })
}