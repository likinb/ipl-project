const csvToJson = require('csvtojson');
const fileSystem = require('fs');


exports.bestEconomyInSuperover = function (inputFilePath, outputFilePath) {
    var economyOfBowlers = {};
    csvToJson().fromFile(inputFilePath).then(runData => {
        runData.forEach(match => {
            var bowlerName = match['bowler'];
            var totalRuns = parseInt(match['total_runs']) - parseInt(match['bye_runs']) - parseInt(match['legbye_runs']);
            if (match['is_super_over'] === '1') {
                if (economyOfBowlers[bowlerName] && economyOfBowlers[bowlerName] !== null) {
                    economyOfBowlers[bowlerName]['runs'] += totalRuns;
                    economyOfBowlers[bowlerName]['balls'] += 1;
                } else {
                    economyOfBowlers[bowlerName] = {
                        'runs': parseInt(totalRuns),
                        'balls': 1
                    };
                }
            }
        })
        var economyBowlers = {};
        var economyArr = [];
        for (var bowlerName in economyOfBowlers) {
            var o = parseInt(economyOfBowlers[bowlerName].balls);
            var over = o / 6;
            var economy = parseInt(economyOfBowlers[bowlerName].runs) / over;
            economyArr.push({ BowlerName: bowlerName, Economy: economy });
        }
        economyArr = economyArr.sort(function (a, b) { return a.Economy - b.Economy; });
        economyArr = economyArr.slice(0, 1);
        fileSystem.writeFile(outputFilePath, JSON.stringify(economyArr), function (err) {
            if (err) return console.log(err);
            console.log('Count of batsman dismissed by a bowler is updated at ' + outputFilePath);
        });
    })
}