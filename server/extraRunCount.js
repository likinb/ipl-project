const csvToJson = require('csvtojson');
const fileSystem = require('fs');

var extraCount = {};
var matchIDs = [];

exports.extraRunsConcededByTeam = function (inputFilePath, inputFileDeliveries, outputFilePath) {
    csvToJson({ checkType: true }).fromFile(inputFilePath).then(matchesData => {
        var matchIDs = matchesData.map(match => { 
            if (match['season'] == 2016) { return match['id'];}
        });
        console.log(matchIDs);
        csvToJson({ checkType: true }).fromFile(inputFileDeliveries).then(runData => {
            runData.forEach(ball2Ball => {
                var matchId = ball2Ball['match_id'];
                if (matchIDs.includes(matchId)) {
                    var team = ball2Ball['bowling_team'];
                    var extraRuns = ball2Ball['extra_runs'];
                    if (extraCount[team] >= 0) {
                        extraCount[team] += extraRuns;
                    } else {
                        extraCount[team] = 0;
                    }
                }
            })
            fileSystem.writeFile(outputFilePath, JSON.stringify(extraCount), function (err) {
                if (err) return console.log(err);
                console.log('Extra runs conceded by all teams in 2016 is updated at ' + outputFilePath);
            });
        })
    })
}

