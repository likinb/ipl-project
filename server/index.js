const matchesInASeason = require('./matchCount');
const totalMatchesWon = require('./teamWinCount');
const totalExtras = require('./extraRunCount');
const bestEconomy = require('./economy');
const tossAndmatch = require('./tossAndMatchCount');
const manOfTheMatch = require('./manOfTheMatchCount');
const strikeRateVKohli = require('./strikeRate');
const dismissalRate = require('./dismissalCount');
const bestSuperOverEconomy = require('./economyInSuperover');

// matchesInASeason.matchesPerSeason('../data/matches.csv','../output/matchesPerYear.json');
// totalMatchesWon.totalMatchesWonByATeam('../data/matches.csv', '../output/totalMatchesWon.json');
totalExtras.extraRunsConcededByTeam('../data/matches.csv', '../data/deliveries.csv', '../output/totalExtrasByTeam.json');
// bestEconomy.top10Bowlers('../data/matches.csv', '../data/deliveries.csv', '../output/Economy.json');
// tossAndmatch.tossAndMatchWon('../data/matches.csv', '../output/tossAndMatchWon.json');
// manOfTheMatch.maxMOMInASeason('../data/matches.csv', '../output/maxMomInSeason.json');
// strikeRateVKohli.strikeRateOfVKohli('../data/matches.csv', '../data/deliveries.csv', '../output/playerStrikeRatePerYear.json');
// dismissalRate.batsmanDismissalCount('../data/deliveries.csv', '../output/dismissalCount.json');
// bestSuperOverEconomy.bestEconomyInSuperover('../data/deliveries.csv', '../output/topEconomyInSuperover.json');