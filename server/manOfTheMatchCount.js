const csvToJson = require('csvtojson');
const fileSystem = require('fs');

exports.maxMOMInASeason = function (inputFilePath, outputFilePath) {
    let momCount = {};
    let prevYear = 0;
    let maxMomCountPerSeason = {};
    csvToJson().fromFile(inputFilePath).then(matchesData => {
        matchesData.forEach(match => {
            let year = match['season'];
            let playerName = match['player_of_match'];
            if (prevYear != year) {
                let max = 0;
                for (let player in momCount) {
                    if (momCount[player] >= max) {
                        if (max == momCount[player]) {
                            maxMomCountPerSeason[prevYear].push(player);
                        } else {
                            maxMomCountPerSeason[prevYear] = [player];
                            max = momCount[player];
                        }
                    }
                }
                momCount = {};
                prevYear = year;
                momCount[playerName] = 1;
            } else {
                if (playerName in momCount) {
                    momCount[playerName] += 1;
                } else {
                    momCount[playerName] = 1;
                }
            }

        })
        fileSystem.writeFile(outputFilePath, JSON.stringify(maxMomCountPerSeason), function (err) {
            if (err) return console.log(err);
            console.log('Most number of Man of the Match awards in a season is updated at ' + outputFilePath);
        });
    })
}