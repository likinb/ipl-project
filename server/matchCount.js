const csvToJson = require('csvtojson');
const fileSystem = require('fs');


exports.matchesPerSeason = function (inputFilePath, outputFilePath) {

    csvToJson({ checktype: true }).fromFile(inputFilePath).then(matchesData => {
        var yearCount = matchesData.reduce((acculumator, current) => {
            var year = current['season'];
            if (acculumator[year] >= 0) {
                acculumator[year] += 1;
            } else {
                acculumator[year] = 1;
            }
            return acculumator;
        }, {})
        fileSystem.writeFile(outputFilePath, JSON.stringify(yearCount), function (err) {
            if (err) return console.log(err);
            console.log('No. of matches in a IPL season is updated at ' + outputFilePath);
        });
    })
}
