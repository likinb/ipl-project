var http = require('http');
var fs = require('fs');
http.createServer((request, response) => {
    var url = request.url;
    if (url === '/') {
        fs.readFile('./htmlindex.html', 'UTF-8', function (err, data) {
            if (err) {
                console.log('You have successfully encountered an error...');
                console.log(err);
                return;
            }
            response.writeHead(200, { "Content-Type": 'text/html' });
            response.write(data);
            response.end();
        });
    } else {
        var filePath = '../output/' + url + '.json';
        fs.readFile(filePath, 'UTF-8', function (err, data) {
            if (err) {
                if (err.code == 'ENOENT') {
                    response.write('Oops!!! File is not found in the server.');
                    response.write(' Please check the name or try a different file');
                    response.end();
                } else {
                    console.log('You have successfully encountered an error...');
                    console.log(err);
                }

            } else {
                response.writeHead(200, { "Content-Type": 'text/JSON' });
                response.write(data);
                response.end();
            }
        });
    }
}).listen(7549);