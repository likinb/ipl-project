const csvToJson = require('csvtojson');
const fileSystem = require('fs');

exports.strikeRateOfVKohli = function (inputFilePath, inputFileDeliveries, outputFilePath) {
    matchesIdPerYear = {};
    batsManData = {};
    player = 'V Kohli';
    csvToJson().fromFile(inputFilePath).then(
        source => {
            source.forEach(match => {
                let year = match['season'];
                let id = match['id'];
                matchesIdPerYear[id] = year;
            });
            csvToJson().fromFile(inputFileDeliveries).then(
                source1 => {
                    source1.forEach(ballData => {
                        let batsman = ballData['batsman'];
                        if (batsman === player) {
                            let id = ballData['match_id'];
                            let wideball = ballData['wide_runs'];
                            let noball = ballData['noball_runs'];
                            let runs = parseInt(ballData['batsman_runs']);
                            let ball = 1;
                            if (matchesIdPerYear[id] in batsManData) {
                                if (noball > 0 || wideball > 0) {
                                    ball = 0;
                                }
                                batsManData[matchesIdPerYear[id]].balls += ball;
                                batsManData[matchesIdPerYear[id]].toatl_runs += runs;
                            } else {
                                if (noball > 0 || wideball > 0) {
                                    ball = 0;
                                }
                                batsManData[matchesIdPerYear[id]] = {
                                    balls: ball,
                                    toatl_runs: runs
                                };
                            }
                        }
                    });
                    for (let item in batsManData) {
                        let balls = batsManData[item].balls;
                        let runs = batsManData[item].toatl_runs;
                        let strikerate = ((runs / balls) * 100).toFixed(2);
                        batsManData[item] = {
                            strike_rate: strikerate
                        };
                    }
                    let playerStrikeRateData = {};
                    playerStrikeRateData["Strike Rate of " + player] = batsManData;
                    fileSystem.writeFile(outputFilePath, JSON.stringify(playerStrikeRateData), function (err) {
                        if (err) return console.log(err);
                        console.log('Strike rate of V Kohli in each season is updated at ' + outputFilePath);
                    });
                })
        })
}