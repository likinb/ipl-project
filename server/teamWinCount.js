const csvToJson = require('csvtojson');
const fileSystem = require('fs');



exports.totalMatchesWonByATeam = function(inputFilePath, outputFilePath) {
    csvToJson().fromFile(inputFilePath).then(matchesData => {
        var winCount = {};
        matchesData.forEach(match => {
            var teamWon = match['winner'];
            var year=match['season'];
            var teamWonInSeason = teamWon + ' ' + year;
            if (teamWonInSeason === '') {
                tteamWonInSeason='';
            }
            else if (winCount[teamWonInSeason] >= 0) {
                winCount[teamWonInSeason]+=1;
            } else {
                winCount[teamWonInSeason]=1;
            }
        })
        fileSystem.writeFile(outputFilePath, JSON.stringify(winCount), function (err) {
            if (err) return console.log(err);
            console.log('No. of matches won by all the teams in a season is updated at '+outputFilePath);
          });
    })
}

