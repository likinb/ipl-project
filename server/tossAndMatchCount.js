const csvToJson = require('csvtojson');
const fileSystem = require('fs');



exports.tossAndMatchWon = function (inputFilePath, outputFilePath) {
    var tossAndMatchCount = {};
    csvToJson().fromFile(inputFilePath).then(matchesData => {
        matchesData.forEach(i => {
            var teamName = i['winner'];
            if (tossAndMatchCount[teamName] >= 0) {
                if (teamName === i['toss_winner'] && teamName !== '')
                    tossAndMatchCount[teamName] += 1;
            } else {
                tossAndMatchCount[teamName] = 0;
                if (teamName === i['toss_winner'])
                    tossAndMatchCount[teamName] += 1;
            }
        })
        fileSystem.writeFile(outputFilePath, JSON.stringify(tossAndMatchCount), function (err) {
            if (err) return console.log(err);
            console.log('No of times a team won both toss and match is updated at ' + outputFilePath);
        });
    })
}
